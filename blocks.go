package ixblue

import (
	"encoding"
	"fmt"
	"math"
	"strconv"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
)

// Bitmasks for navigation data blocks
//
type DataSelector uint64

// Split a DataSelector into a block group number and bitmask
func (d DataSelector) Split() (DataSelector, uint32) {
	return (d & 0xf00000000), uint32(d & 0xffffffff)
}

// Standard data blocks
const StdBlockGroup DataSelector = 0x0
const (
	BmAttitudeHdg       DataSelector = 1 << 0
	BmAttitudeHdgStddev              = 1 << 1
	BmRtHeave                        = 1 << 2
	BmSmartHeave                     = 1 << 3
	BmHRPRate                        = 1 << 4
	BmVehRotRate                     = 1 << 5
	BmVehAccel                       = 1 << 6
	BmPosition                       = 1 << 7
	BmPositionStddev                 = 1 << 8
	BmGeoSpeed                       = 1 << 9
	BmGeoSpeedStddev                 = 1 << 10
	BmCurrent                        = 1 << 11
	BmCurrentStddev                  = 1 << 12
	BmSysDate                        = 1 << 13
	BmSensorStatus                   = 1 << 14
	BmAlgStatus                      = 1 << 15
	BmSysStatus                      = 1 << 16
	BmUserStatus                     = 1 << 17
	BmHeaveSpeed                     = 1 << 21
	BmVehSpeed                       = 1 << 22
	BmGeoAccel                       = 1 << 23
	BmCourseSpeed                    = 1 << 24
	BmTemperature                    = 1 << 25
	BmQuaternion                     = 1 << 26
	BmQuaternionStddev               = 1 << 27
	BmRawAccel                       = 1 << 28
	BmRawAccelStddev                 = 1 << 29
	BmRotRateStddev                  = 1 << 30
)

// Extended data blocks
const ExtBlockGroup DataSelector = 0x100000000
const (
	BmVehRotAccel       DataSelector = (1 << 0) | ExtBlockGroup
	BmVehRotAccelStddev              = (1 << 1) | ExtBlockGroup
	BmRawVehRot                      = (1 << 2) | ExtBlockGroup
)

// External data blocks
const ExternBlockGroup DataSelector = 0x200000000
const (
	BmUtcData        DataSelector = (1 << 0) | ExternBlockGroup
	BmGnss1Data                   = (1 << 1) | ExternBlockGroup
	BmGnss2Data                   = (1 << 2) | ExternBlockGroup
	BmManualGnss                  = (1 << 3) | ExternBlockGroup
	BmEmlog1Data                  = (1 << 4) | ExternBlockGroup
	BmEmlog2Data                  = (1 << 5) | ExternBlockGroup
	BmUsbl1Data                   = (1 << 6) | ExternBlockGroup
	BmUsbl2Data                   = (1 << 7) | ExternBlockGroup
	BmUsbl3Data                   = (1 << 8) | ExternBlockGroup
	BmDepthData                   = (1 << 9) | ExternBlockGroup
	BmDvlGndSpeed                 = (1 << 10) | ExternBlockGroup
	BmDvlWaterSpeed               = (1 << 11) | ExternBlockGroup
	BmSoundVel                    = (1 << 12) | ExternBlockGroup
	BmLbl1Data                    = (1 << 14) | ExternBlockGroup
	BmLbl2Data                    = (1 << 15) | ExternBlockGroup
	BmLbl3Data                    = (1 << 16) | ExternBlockGroup
	BmLbl4Data                    = (1 << 17) | ExternBlockGroup
	BmDvl2GndSpeed                = (1 << 21) | ExternBlockGroup
	BmDvl2WaterSpeed              = (1 << 22) | ExternBlockGroup
)

// Map data selectors to data block sizes
var blockSizes = map[DataSelector]int{
	BmAttitudeHdg:       12,
	BmAttitudeHdgStddev: 12,
	BmRtHeave:           16,
	BmSmartHeave:        8,
	BmHRPRate:           12,
	BmVehRotRate:        12,
	BmVehAccel:          12,
	BmPosition:          21,
	BmPositionStddev:    16,
	BmGeoSpeed:          12,
	BmGeoSpeedStddev:    12,
	BmCurrent:           8,
	BmCurrentStddev:     8,
	BmSysDate:           4,
	BmSensorStatus:      8,
	BmAlgStatus:         16,
	BmSysStatus:         12,
	BmUserStatus:        4,
	BmHeaveSpeed:        12,
	BmVehSpeed:          12,
	BmGeoAccel:          12,
	BmCourseSpeed:       8,
	BmTemperature:       12,
	BmQuaternion:        16,
	BmQuaternionStddev:  12,
	BmRawAccel:          12,
	BmRawAccelStddev:    12,
	BmRotRateStddev:     12,
	BmVehRotAccel:       12,
	BmVehRotAccelStddev: 12,
	BmRawVehRot:         12,
	BmUtcData:           5,
	BmGnss1Data:         46,
	BmGnss2Data:         46,
	BmManualGnss:        46,
	BmEmlog1Data:        13,
	BmEmlog2Data:        13,
	BmUsbl1Data:         49,
	BmUsbl2Data:         49,
	BmUsbl3Data:         49,
	BmDepthData:         12,
	BmDvlGndSpeed:       37,
	BmDvlWaterSpeed:     33,
	BmSoundVel:          8,
	BmLbl1Data:          41,
	BmLbl2Data:          41,
	BmLbl3Data:          41,
	BmLbl4Data:          41,
	BmDvl2GndSpeed:      37,
	BmDvl2WaterSpeed:    33,
}

// DataBlock is the interface implemented by an object which
// contains the values from a data block with a data record.
type DataBlock interface {
	encoding.BinaryUnmarshaler
	Bitmask() DataSelector
}

// SensorBlock is implemented by an object which contains
// sensor data to send to the INS
type SensorBlock interface {
	encoding.BinaryMarshaler
	Bitmask() DataSelector
}

// Data block structures
//
// Attitude coordinates
type AttCoord struct {
	Heading float32 `json:"heading"`
	Roll    float32 `json:"roll"`
	Pitch   float32 `json:"pitch"`
}

func (b *AttCoord) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Heading = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Roll = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Pitch = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

// Attitude and Heading in the vehicle reference frame
type AttitudeHdg struct {
	AttCoord
}

func (b *AttitudeHdg) Bitmask() DataSelector {
	return BmAttitudeHdg
}

// Standard deviation of attitude and heading
type AttitudeHdgStddev struct {
	AttCoord
}

func (b *AttitudeHdgStddev) Bitmask() DataSelector {
	return BmAttitudeHdgStddev
}

// Realtime heave data
type RtHeave struct {
	Heave      float32 `json:"heave"`
	HeaveLever float32 `json:"heave_lever"`
	SurgeLever float32 `json:"surge_lever"`
	SwayLever  float32 `json:"sway_lever"`
}

func (b *RtHeave) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Heave = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.HeaveLever = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.SurgeLever = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.SwayLever = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *RtHeave) Bitmask() DataSelector {
	return BmRtHeave
}

// Time derivative of heading, roll, and pitch
type HRPRate struct {
	AttCoord
}

func (b *HRPRate) Bitmask() DataSelector {
	return BmHRPRate
}

// Vehicle coordinates
type VehCoord struct {
	Xv1 float32 `json:"xv1"`
	Xv2 float32 `json:"xv2"`
	Xv3 float32 `json:"xv3"`
}

func (b *VehCoord) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Xv1 = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Xv2 = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Xv3 = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

// Rotation rate in vehicle coordinates
type VehRotRate struct {
	VehCoord
}

func (b *VehRotRate) Bitmask() DataSelector {
	return BmVehRotRate
}

// Acceleration in vehicle frame at the primary lever arm, gravity compensated.
type VehAccel struct {
	VehCoord
}

func (b *VehAccel) Bitmask() DataSelector {
	return BmVehAccel
}

// Position in WGS84 frame
type Position struct {
	Lat    float64 `json:"lat"`
	Lon    float64 `json:"lon"`
	AltRef uint8   `json:"alt_ref"`
	Alt    float32 `json:"alt"`
}

func (b *Position) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Lat = math.Float64frombits(ByteOrder.Uint64(p[offset:]))
	offset += 8
	b.Lon = math.Float64frombits(ByteOrder.Uint64(p[offset:]))
	offset += 8
	b.AltRef = p[offset]
	offset++
	b.Alt = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *Position) Bitmask() DataSelector {
	return BmPosition
}

// Position standard deviation
type PositionStddev struct {
	North  float32 `json:"north"`
	East   float32 `json:"east"`
	NECorr float32 `json:"ne_corr"`
	Alt    float32 `json:"alt"`
}

func (b *PositionStddev) UnmarshalBinary(p []byte) error {
	offset := 0
	b.North = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.East = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.NECorr = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Alt = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *PositionStddev) Bitmask() DataSelector {
	return BmPositionStddev
}

// Geographical coordinates
type GeoCoord struct {
	North float32 `json:"north"`
	East  float32 `json:"east"`
	Up    float32 `json:"up"`
}

func (b *GeoCoord) UnmarshalBinary(p []byte) error {
	offset := 0
	b.North = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.East = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Up = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

// Speed in the geographic frame at the primary lever arm
type GeoSpeed struct {
	GeoCoord
}

func (b *GeoSpeed) Bitmask() DataSelector {
	return BmGeoSpeed
}

// Standard deviation of GeoSpeed
type GeoSpeedStddev struct {
	GeoCoord
}

func (b *GeoSpeedStddev) Bitmask() DataSelector {
	return BmGeoSpeedStddev
}

// Water current estimate
type Current struct {
	North float32 `json:"north"`
	East  float32 `json:"east"`
}

func (b *Current) UnmarshalBinary(p []byte) error {
	offset := 0
	b.North = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.East = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *Current) Bitmask() DataSelector {
	return BmCurrent
}

// System Date
type SysDate struct {
	Day   uint8  `json:"day"`
	Month uint8  `json:"month"`
	Year  uint16 `json:"year"`
}

func (b *SysDate) UnmarshalBinary(p []byte) error {
	b.Day = p[0]
	b.Month = p[1]
	b.Year = ByteOrder.Uint16(p[2:])
	return nil
}

func (b *SysDate) Bitmask() DataSelector {
	return BmSysDate
}

func (b *SysDate) AsTime() time.Time {
	return time.Date(int(b.Year), time.Month(b.Month), int(b.Day),
		0, 0, 0, 0, time.UTC)
}

type SensorStatus struct {
	Word [2]uint32 `json:"word"`
}

func (b *SensorStatus) UnmarshalBinary(p []byte) error {
	offset := 0
	for i := 0; i < len(b.Word); i++ {
		b.Word[i] = ByteOrder.Uint32(p[offset:])
		offset += 4
	}
	return nil
}

func (b *SensorStatus) Bitmask() DataSelector {
	return BmSensorStatus
}

// Bits from AlgStatus.Word[0]
const (
	AlgGpsReceived       uint32 = (1 << 12)
	AlgGpsValid                 = (1 << 13)
	AlgDvlReceived              = (1 << 8)
	AlgDvlValid                 = (1 << 9)
	AlgDepthReceived            = (1 << 20)
	AlgDepthValid               = (1 << 21)
	AlgManualGpsReceived        = (1 << 28)
	AlgManualGpsValid           = (1 << 29)
)

type AlgStatus struct {
	Word [4]uint32 `json:"word"`
}

func (b *AlgStatus) UnmarshalBinary(p []byte) error {
	offset := 0
	for i := 0; i < len(b.Word); i++ {
		b.Word[i] = ByteOrder.Uint32(p[offset:])
		offset += 4
	}
	return nil
}

func (b *AlgStatus) Bitmask() DataSelector {
	return BmAlgStatus
}

type SysStatus struct {
	Word [4]uint32 `json:"word"`
}

func (b *SysStatus) UnmarshalBinary(p []byte) error {
	offset := 0
	for i := 0; i < len(b.Word); i++ {
		b.Word[i] = ByteOrder.Uint32(p[offset:])
		offset += 4
	}
	return nil
}

func (b *SysStatus) Bitmask() DataSelector {
	return BmSysStatus
}

type UserStatus struct {
	Word [1]uint32 `json:"word"`
}

func (b *UserStatus) UnmarshalBinary(p []byte) error {
	offset := 0
	for i := 0; i < len(b.Word); i++ {
		b.Word[i] = ByteOrder.Uint32(p[offset:])
		offset += 4
	}
	return nil
}

func (b *UserStatus) Bitmask() DataSelector {
	return BmUserStatus
}

// Time rate of change of heave, surge, sway
type HeaveSpeed struct {
	Heave float32 `json:"xv1"`
	Surge float32 `json:"xv2"`
	Sway  float32 `json:"xv3"`
}

func (b *HeaveSpeed) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Heave = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Surge = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Sway = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *HeaveSpeed) Bitmask() DataSelector {
	return BmHeaveSpeed
}

// Speed in vehicle coordinates at the primary lever arm
type VehSpeed struct {
	VehCoord
}

func (b *VehSpeed) Bitmask() DataSelector {
	return BmVehSpeed
}

// Accelerations at the primary lever arm in the geographical frame, not
// gravity compensated.
type GeoAccel struct {
	GeoCoord
}

func (b *GeoAccel) Bitmask() DataSelector {
	return BmGeoAccel
}

// Internal temperatures
type Temperature struct {
	Fog  float32 `json:"fog"`
	Acc  float32 `json:"acc"`
	Sens float32 `json:"sens"`
}

func (b *Temperature) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Fog = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Acc = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Sens = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *Temperature) Bitmask() DataSelector {
	return BmTemperature
}

type RawAccel struct {
	VehCoord
}

func (b *RawAccel) Bitmask() DataSelector {
	return BmRawAccel
}

type RawAccelStddev struct {
	VehCoord
}

func (b *RawAccelStddev) Bitmask() DataSelector {
	return BmRawAccelStddev
}

type VehRotAccel struct {
	VehCoord
}

func (b *VehRotAccel) Bitmask() DataSelector {
	return BmVehRotAccel
}

type VehRotAccelStddev struct {
	VehCoord
}

func (b *VehRotAccelStddev) Bitmask() DataSelector {
	return BmVehRotAccelStddev
}

type RawVehRot struct {
	VehCoord
}

func (b *RawVehRot) Bitmask() DataSelector {
	return BmRawVehRot
}

// Gnss quality values
const (
	QualInvalid uint8 = iota
	QualNatural
	QualDiff
	QualMil
	QualRtk
	QualFloatRtk
)

type GnssData struct {
	// Timestamp in 100us ticks
	Tvalid int32 `json:"tvalid"`
	Id     uint8 `json:"id"`
	Qual   uint8 `json:"qual"`
	// Latitude in degrees; -90, +90
	Lat float64 `json:"lat"`
	// Longitude in degrees E; 0, 360
	Lon float64 `json:"lon"`
	// Geoid (MSL) referenced altitude
	Alt float32 `json:"alt"`
	// Latitude standard deviation in meters
	LatStddev float32 `json:"lat_stddev"`
	// Longitude standard deviation in meters
	LonStddev float32 `json:"lon_stddev"`
	// Latitude-Longitude covariance in meters^2
	LatLonCov float32 `json:"latlon_cov"`
	// Distance between geoid and ellipsoid in meters
	GeoidSep float32 `json:"geoid"`
}

func (b *GnssData) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Tvalid = int32(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Id = p[offset]
	offset++
	b.Qual = p[offset]
	offset++
	b.Lat = math.Float64frombits(ByteOrder.Uint64(p[offset:]))
	offset += 8
	b.Lon = math.Float64frombits(ByteOrder.Uint64(p[offset:]))
	offset += 8
	b.Alt = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.LatStddev = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.LonStddev = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.LatLonCov = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.GeoidSep = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *GnssData) MarshalBinary() ([]byte, error) {
	p := make([]byte, blockSizes[BmGnss1Data])
	offset := 0
	ByteOrder.PutUint32(p[offset:], uint32(b.Tvalid))
	offset += 4
	p[offset] = b.Id
	offset++
	p[offset] = b.Qual
	offset++
	ByteOrder.PutUint64(p[offset:], math.Float64bits(b.Lat))
	offset += 8
	ByteOrder.PutUint64(p[offset:], math.Float64bits(b.Lon))
	offset += 8
	ByteOrder.PutUint32(p[offset:], math.Float32bits(b.Alt))
	offset += 4
	ByteOrder.PutUint32(p[offset:], math.Float32bits(b.LatStddev))
	offset += 4
	ByteOrder.PutUint32(p[offset:], math.Float32bits(b.LonStddev))
	offset += 4
	ByteOrder.PutUint32(p[offset:], math.Float32bits(b.LatLonCov))
	offset += 4
	ByteOrder.PutUint32(p[offset:], math.Float32bits(b.GeoidSep))

	return p, nil
}

// Parse decimal degrees from the string representation of
// degrees and decimal minutes
func dmToDegrees(deg, dm, hemi string) float64 {
	ideg, err := strconv.ParseUint(deg, 10, 16)
	if err != nil {
		return 0.
	}
	min, err := strconv.ParseFloat(dm, 64)
	if err != nil {
		return 0.
	}

	var sign float64
	if hemi == "W" || hemi == "S" {
		sign = -1
	} else {
		sign = 1
	}

	return (float64(ideg) + min/60.) * sign
}

func hmsToSecs(hms string) int32 {
	h, _ := strconv.ParseInt(hms[0:2], 10, 32)
	m, _ := strconv.ParseInt(hms[2:4], 10, 32)
	s, _ := strconv.ParseInt(hms[4:], 10, 32)

	return int32(s + 60*(m+60*h))
}

// UnmarshalNMEA fills the values of a GnssData struct from the contents of a
// GPGGA NMEA sentence.
func (b *GnssData) UnmarshalNMEA(s nmea.Sentence) error {
	if s.Id != "GPGGA" {
		return fmt.Errorf("Wrong sentence type: %s", s.Id)
	}
	b.Tvalid = hmsToSecs(s.Fields[0]) * 10000
	b.Lat = dmToDegrees(s.Fields[1][0:2], s.Fields[1][2:], s.Fields[2])
	b.Lon = dmToDegrees(s.Fields[3][0:3], s.Fields[3][3:], s.Fields[4])
	if b.Lon < 0 {
		b.Lon += 360
	}
	q, _ := strconv.Atoi(s.Fields[5])
	b.Qual = uint8(q)
	hdop, _ := strconv.ParseFloat(s.Fields[7], 32)
	b.LatStddev = float32(hdop) * 6
	b.LonStddev = b.LatStddev
	x, _ := strconv.ParseFloat(s.Fields[8], 32)
	b.Alt = float32(x)
	x, _ = strconv.ParseFloat(s.Fields[10], 32)
	b.GeoidSep = float32(x)

	return nil
}

type Gnss1Data struct {
	GnssData
}

func (b *Gnss1Data) Bitmask() DataSelector {
	return BmGnss1Data
}

type Gnss2Data struct {
	GnssData
}

func (b *Gnss2Data) Bitmask() DataSelector {
	return BmGnss2Data
}

type ManualGnss struct {
	GnssData
}

func (b *ManualGnss) Bitmask() DataSelector {
	return BmManualGnss
}

type DepthData struct {
	Tvalid int32   `json:"tvalid"`
	Depth  float32 `json:"depth"`
	Stddev float32 `json:"stddev"`
}

func (b *DepthData) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Tvalid = int32(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Depth = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Stddev = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *DepthData) MarshalBinary() ([]byte, error) {
	p := make([]byte, blockSizes[BmDepthData])
	offset := 0
	ByteOrder.PutUint32(p[offset:], uint32(b.Tvalid))
	offset += 4
	ByteOrder.PutUint32(p[offset:], math.Float32bits(b.Depth))
	offset += 4
	ByteOrder.PutUint32(p[offset:], math.Float32bits(b.Stddev))
	return p, nil
}

func (b *DepthData) Bitmask() DataSelector {
	return BmDepthData
}

// DVL speed relative to the bottom
type DvlGndSpeed struct {
	Tvalid      int32      `json:"tvalid"`
	Id          uint8      `json:"id"`
	Speed       [3]float32 `json:"speed"`
	Svel        float32    `json:"svel"`
	Alt         float32    `json:"alt"`
	SpeedStddev [3]float32 `json:"speed_stddev"`
}

func (b *DvlGndSpeed) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Tvalid = int32(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Id = p[offset]
	offset++
	for i := 0; i < 3; i++ {
		b.Speed[i] = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
		offset += 4
	}
	b.Svel = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Alt = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	for i := 0; i < 3; i++ {
		b.SpeedStddev[i] = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
		offset += 4
	}

	return nil
}

func (b *DvlGndSpeed) Bitmask() DataSelector {
	return BmDvlGndSpeed
}

// DVL speed relative to the water
type DvlWaterSpeed struct {
	Tvalid      int32      `json:"tvalid"`
	Id          uint8      `json:"id"`
	Speed       [3]float32 `json:"speed"`
	Svel        float32    `json:"svel"`
	SpeedStddev [3]float32 `json:"speed_stddev"`
}

func (b *DvlWaterSpeed) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Tvalid = int32(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Id = p[offset]
	offset++
	for i := 0; i < 3; i++ {
		b.Speed[i] = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
		offset += 4
	}
	b.Svel = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	offset += 4
	for i := 0; i < 3; i++ {
		b.SpeedStddev[i] = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
		offset += 4
	}

	return nil
}

func (b *DvlWaterSpeed) Bitmask() DataSelector {
	return BmDvlWaterSpeed
}

type SoundVel struct {
	Tvalid int32   `json:"tvalid"`
	Svel   float32 `json:"svel"`
}

func (b *SoundVel) UnmarshalBinary(p []byte) error {
	offset := 0
	b.Tvalid = int32(ByteOrder.Uint32(p[offset:]))
	offset += 4
	b.Svel = math.Float32frombits(ByteOrder.Uint32(p[offset:]))
	return nil
}

func (b *SoundVel) MarshalBinary() ([]byte, error) {
	p := make([]byte, blockSizes[BmSoundVel])
	offset := 0
	ByteOrder.PutUint32(p[offset:], uint32(b.Tvalid))
	offset += 4
	ByteOrder.PutUint32(p[offset:], math.Float32bits(b.Svel))

	return p, nil
}

func (b *SoundVel) Bitmask() DataSelector {
	return BmSoundVel
}
