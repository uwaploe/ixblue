// Package ixblue provides functions and data structures for communicating
// with an iXBlue Rovins inertial navigation system (INS).
package ixblue

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

// Byte order for all binary messages
var ByteOrder binary.ByteOrder = binary.BigEndian

var (
	ErrNotFound = errors.New("Data block not found")
	ErrInvalid  = errors.New("Invalid data block selector")
)

// The following structures are for version 3 of the iXBlue Standard Binary
// Format.
//
// First three bytes identify the packet type
var NavV3Type = [3]byte{'I', 'X', 3}

// Command (and Response) header
type CmdHeader struct {
	Size uint16
}

type Blocks struct {
	DataMask   uint32
	ExtMask    uint32
	ExternMask uint32
}

// Navigation output header
type NavHeader struct {
	Blocks
	Size    uint16
	Tod     uint32
	Counter uint32
}

var NavHeaderLen = binary.Size(NavHeader{})

func (h NavHeader) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})
	m["data_blocks"] = fmt.Sprintf("%08x", h.DataMask)
	m["ext_blocks"] = fmt.Sprintf("%08x", h.ExtMask)
	m["extern_blocks"] = fmt.Sprintf("%08x", h.ExternMask)
	m["size"] = h.Size
	m["tod"] = h.Tod
	m["counter"] = h.Counter
	return json.Marshal(m)
}

func (h *NavHeader) UnmarshalBinary(b []byte) error {
	offset := 0
	h.DataMask = ByteOrder.Uint32(b[offset:])
	offset += 4
	h.ExtMask = ByteOrder.Uint32(b[offset:])
	offset += 4
	h.ExternMask = ByteOrder.Uint32(b[offset:])
	offset += 4
	h.Size = ByteOrder.Uint16(b[offset:])
	offset += 2
	h.Tod = ByteOrder.Uint32(b[offset:])
	offset += 4
	h.Counter = ByteOrder.Uint32(b[offset:])

	return nil
}

func (h NavHeader) MarshalBinary() ([]byte, error) {
	b := make([]byte, NavHeaderLen)
	offset := 0
	ByteOrder.PutUint32(b[offset:], h.DataMask)
	offset += 4
	ByteOrder.PutUint32(b[offset:], h.ExtMask)
	offset += 4
	ByteOrder.PutUint32(b[offset:], h.ExternMask)
	offset += 4
	ByteOrder.PutUint16(b[offset:], h.Size)
	offset += 2
	ByteOrder.PutUint32(b[offset:], h.Tod)
	offset += 4
	ByteOrder.PutUint32(b[offset:], h.Counter)
	return b, nil
}

// BlockSize returns the total size of a data block group
func (b Blocks) BlockSize(group DataSelector) int {
	var (
		blockmask uint32
		size      int
	)

	switch group {
	case StdBlockGroup:
		blockmask = b.DataMask
	case ExtBlockGroup:
		blockmask = b.ExtMask
	case ExternBlockGroup:
		blockmask = b.ExternMask
	}

	for i := 0; i < 32; i++ {
		mask := uint32(1 << i)
		if (mask & blockmask) != 0 {
			size += blockSizes[DataSelector(mask)|group]
		}
	}

	return size
}

// OffsetOf returns the byte offset within the data section of the data
// block identified by sel.
func (b Blocks) OffsetOf(sel DataSelector) (int, error) {
	group, mask := sel.Split()
	var blockmask uint32

	offset := 0
	// Add up the size of all groups preceeding the selection
	switch group {
	case StdBlockGroup:
		blockmask = b.DataMask
	case ExtBlockGroup:
		blockmask = b.ExtMask
		offset = b.BlockSize(StdBlockGroup)
	case ExternBlockGroup:
		blockmask = b.ExternMask
		offset = b.BlockSize(StdBlockGroup) + b.BlockSize(ExtBlockGroup)
	default:
		return -1, ErrInvalid
	}
	if (mask & blockmask) == 0 {
		return -1, ErrNotFound
	}

	var i uint32
	for i = 1; i < mask; i = i << 1 {
		if (i & blockmask) != 0 {
			offset += blockSizes[DataSelector(i)|group]
		}
	}

	return offset, nil
}

// Ticks converts a time.Time value to time-of-day in 100us ticks
func Ticks(t time.Time) int32 {
	dt := t.Sub(t.Truncate(24 * time.Hour))
	return int32(dt.Microseconds() / 100)
}

// Navigation input header
type NavInpHeader struct {
	Blocks
	Size    uint16
	TimeRef uint8
	// padding
	_ [7]uint8
}

func (h NavInpHeader) MarshalBinary() ([]byte, error) {
	b := make([]byte, NavHeaderLen)
	offset := 0
	ByteOrder.PutUint32(b[offset:], h.DataMask)
	offset += 4
	ByteOrder.PutUint32(b[offset:], h.ExtMask)
	offset += 4
	ByteOrder.PutUint32(b[offset:], h.ExternMask)
	offset += 4
	ByteOrder.PutUint16(b[offset:], h.Size)
	offset += 2
	b[offset] = h.TimeRef
	offset++
	zero := b[offset : offset+7]
	for i := range zero {
		zero[i] = 0
	}
	return b, nil
}
