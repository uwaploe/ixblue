package ixblue

import (
	"bytes"
	"encoding/binary"
	"io"
	"reflect"
	"testing"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
)

func TestScan(t *testing.T) {
	check := updateCsum(0, []byte("IX\x03"))
	table := []struct {
		input, match []byte
		csum         uint32
		unread       int
		err          error
	}{
		{
			input:  []byte("IX\x03hello"),
			match:  []byte("IX\x03"),
			csum:   check,
			unread: 5,
		},
		{
			input: []byte("IX"),
			match: []byte("IX\x03"),
			err:   io.EOF,
		},
		{
			input:  []byte("blahblahIIIX\x03hello"),
			match:  []byte("IX\x03"),
			csum:   check,
			unread: 5,
		},
		{
			input:  []byte("blahIXIIX\x03hello"),
			match:  []byte("IX\x03"),
			csum:   check,
			unread: 5,
		},
	}

	for _, e := range table {
		buf := bytes.NewBuffer(e.input)
		csum, err := scanSeq(buf, e.match)
		if err != e.err {
			t.Errorf("Unexpected error: %v", err)
		}

		if csum != e.csum {
			t.Errorf("Checksum error; expected %d, got %d", e.csum, csum)
		}

		if buf.Len() != e.unread {
			t.Errorf("Unexpected read position; %d characters left", buf.Len())
		}
	}
}

func TestOffset(t *testing.T) {
	table := []struct {
		hdr    Blocks
		sel    DataSelector
		offset int
	}{
		{
			hdr:    Blocks{DataMask: 0x7fe3ffff, ExtMask: 0x07, ExternMask: 0x01},
			sel:    BmUtcData,
			offset: 0x182 - binary.Size(NavHeader{}) - 3,
		},
	}

	for _, e := range table {
		offset, err := e.hdr.OffsetOf(e.sel)
		if err != nil {
			t.Fatal(err)
		}
		if offset != e.offset {
			t.Errorf("Bad offset; expected %d, got %d", e.offset, offset)
		}
	}
}

func TestEncodeDecode(t *testing.T) {
	tref, _ := time.Parse("2006 Jan 02 15:04:05", "2012 Dec 07 12:15:30.25")
	ticks := Ticks(tref)
	if ticks != 441302500 {
		t.Fatalf("Wrong tick value: %d", ticks)
	}

	sensors := []SensorBlock{
		&Gnss1Data{
			GnssData: GnssData{
				Tvalid: ticks,
				Id:     1,
				Lat:    47,
				Lon:    238,
			},
		},
		&DepthData{
			Tvalid: ticks,
			Depth:  42,
		},
	}

	var b bytes.Buffer
	enc := NewEncoder(&b)
	enc.Encode(sensors...)
	dec := NewDecoder(&b)
	dr, err := dec.Decode()
	if err != nil {
		t.Fatal(err)
	}

	blocks := []DataBlock{
		&Gnss1Data{},
		&DepthData{},
	}
	err = dr.ExtractBlocks(blocks...)
	if err != nil {
		t.Fatal(err)
	}

	gnss, ok := blocks[0].(*Gnss1Data)
	if !ok {
		t.Fatalf("Unexpected type: %T", blocks[0])
	}

	gnss_expected, ok := sensors[0].(*Gnss1Data)
	if !ok {
		t.Fatalf("Unexpected type: %T", sensors[0])
	}

	if !reflect.DeepEqual(*gnss, *gnss_expected) {
		t.Errorf("Value mismatch; expected %#v, got %#v", *gnss_expected, *gnss)
	}

	depth, ok := blocks[1].(*DepthData)
	if !ok {
		t.Fatalf("Unexpected type: %T", blocks[1])
	}

	depth_expected, ok := sensors[1].(*DepthData)
	if !ok {
		t.Fatalf("Unexpected type: %T", sensors[1])
	}

	if !reflect.DeepEqual(*depth, *depth_expected) {
		t.Errorf("Value mismatch; expected %#v, got %#v", *depth_expected, *depth)
	}
}

func TestReadGGA(t *testing.T) {
	text := "$GPGGA,170834,4124.8963,N,08151.6838,W,1,05,1.5,280.2,M,-34.0,M,,*75"
	expected := GnssData{
		Tvalid:    61714 * 10000,
		Qual:      1,
		Lat:       41 + 24.8963/60,
		Lon:       360 - (81 + 51.6838/60),
		Alt:       280.2,
		LatStddev: 9,
		LonStddev: 9,
		GeoidSep:  -34,
	}

	sentence, err := nmea.ParseSentence([]byte(text))
	if err != nil {
		t.Fatal(err)
	}

	gnss := GnssData{}
	err = gnss.UnmarshalNMEA(sentence)
	if err != nil {
		t.Fatal(err)
	}

	if gnss != expected {
		t.Errorf("Value mismatch; expected %#v, got %#v", expected, gnss)
	}
}
