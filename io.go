package ixblue

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"time"
)

var (
	ErrChecksum = errors.New("Checksum error")
	ErrType     = errors.New("Packet type error")
	ErrInput    = errors.New("Invalid input type")
)

// DataRecord contains an output V3 navigation record from an iXBlue INS.
type DataRecord struct {
	Hdr  NavHeader
	Data []byte
	Csum uint32
}

func (d DataRecord) Timestamp() time.Time {
	date := SysDate{}
	if err := d.ExtractBlocks(&date); err != nil {
		return time.Time{}
	}
	usecs := time.Duration(d.Hdr.Tod) * 100 * time.Microsecond
	return date.AsTime().Add(usecs)
}

func (d DataRecord) MarshalBinary() ([]byte, error) {
	p := make([]byte, d.Hdr.Size)
	offset := 0
	offset += copy(p[offset:], NavV3Type[:])

	if buf, err := d.Hdr.MarshalBinary(); err != nil {
		return nil, err
	} else {
		offset += copy(p[offset:], buf)
	}
	offset += copy(p[offset:], d.Data)
	ByteOrder.PutUint32(p[offset:], d.Csum)

	return p, nil
}

func (d *DataRecord) UnmarshalBinary(b []byte) error {
	if !bytes.Equal(b[0:3], NavV3Type[:]) {
		return ErrType
	}
	offset := 3
	if err := d.Hdr.UnmarshalBinary(b[offset:]); err != nil {
		return err
	}
	offset += NavHeaderLen

	datalen := int(d.Hdr.Size) - NavHeaderLen - 7
	d.Data = make([]byte, datalen)
	offset += copy(d.Data, b[offset:])
	d.Csum = ByteOrder.Uint32(b[offset:])

	return nil
}

func (d DataRecord) WriteTo(w io.Writer) (int64, error) {
	_, err := w.Write(NavV3Type[:])
	if err != nil {
		return 0, err
	}
	err = binary.Write(w, ByteOrder, d.Hdr)
	if err != nil {
		return 0, err
	}
	_, err = w.Write(d.Data)
	if err != nil {
		return 0, err
	}
	err = binary.Write(w, ByteOrder, d.Csum)
	return int64(d.Hdr.Size), err
}

// ExtractBlocks decodes the data in one or more data blocks and stores
// the values into the provided DataBlock objects
func (d DataRecord) ExtractBlocks(blocks ...DataBlock) error {
	for _, block := range blocks {
		offset, err := d.Hdr.OffsetOf(block.Bitmask())
		if err != nil {
			return fmt.Errorf("cannot locate %T: %w", block, err)
		}
		if err := block.UnmarshalBinary(d.Data[offset:]); err != nil {
			return fmt.Errorf("cannot decode %T: %w", block, err)
		}
	}

	return nil
}

// TraceFunc is used to log the contents of incoming and outgoing packets
var TraceFunc func(string, ...interface{})

func readAll(rdr io.Reader, p []byte) (int, error) {
	var (
		err   error
		n, nn int
	)

	total := len(p)
	for n < total {
		nn, err = io.ReadFull(rdr, p[n:])
		n += nn
		if nn == 0 && err != nil {
			break
		}
	}

	return n, err
}

func updateCsum(csum uint32, b []byte) uint32 {
	for i := 0; i < len(b); i++ {
		csum += uint32(b[i])
	}
	return csum
}

func scanSeq(rdr io.Reader, seq []byte) (uint32, error) {
	var csum uint32
	char := make([]byte, 1)

findstart:
	for {
		if _, err := rdr.Read(char); err != nil {
			return csum, err
		}
		if char[0] != seq[0] {
			continue
		}
		csum = uint32(seq[0])
		n := len(seq)
		i := int(1)
		for i < n {
			if _, err := rdr.Read(char); err != nil {
				return 0, err
			}
			switch char[0] {
			case seq[i]:
				csum += uint32(seq[i])
				i++
			case seq[0]:
				i = 1
				csum = uint32(seq[0])
			default:
				continue findstart
			}
		}
		break
	}

	return csum, nil
}

// A Decoder reads and decodes DataRecords from an input stream
type Decoder struct {
	r io.Reader
}

func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{r: r}
}

func (d *Decoder) Decode() (DataRecord, error) {
	dr := DataRecord{}
	// Look for packet start
	csum, err := scanSeq(d.r, NavV3Type[:])
	if err != nil {
		return dr, err
	}

	// Read and decode the rest of the header
	buf := make([]byte, NavHeaderLen)
	_, err = d.r.Read(buf)
	if err != nil {
		return dr, err
	}
	csum = updateCsum(csum, buf)
	err = dr.Hdr.UnmarshalBinary(buf)
	if err != nil {
		return dr, err
	}

	// Read the packet body. The size stored in the header is the
	// total packet size so we need to subtract the header and the
	// 4-byte checksum
	datalen := int(dr.Hdr.Size) - NavHeaderLen - 7
	dr.Data = make([]byte, datalen)
	_, err = readAll(d.r, dr.Data)
	if err != nil {
		return dr, err
	}
	csum = updateCsum(csum, dr.Data)

	// Read the 4-byte checksum
	_, err = d.r.Read(buf[:4])
	if err != nil {
		return dr, err
	}
	checksum := ByteOrder.Uint32(buf[0:4])
	if checksum != csum {
		return dr, ErrChecksum
	}
	dr.Csum = checksum

	return dr, err
}

// An Encoder encodes and writes external sensor data to an iXBlue INS.
type Encoder struct {
	w io.Writer
}

func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w}
}

// Encode builds a data packet from the SensorBlocks and sends it to the
// Writer.
func (e *Encoder) Encode(blocks ...SensorBlock) error {
	var csum uint32

	hdr := NavInpHeader{}
	data := make([]byte, 0)

	// Marshal each block into the data section of the packet and set the
	// corresponding bit in the header bitmask
	for _, block := range blocks {
		group, mask := block.Bitmask().Split()
		if group != ExternBlockGroup {
			return ErrInput
		}
		hdr.ExternMask |= mask
		b, err := block.MarshalBinary()
		if err != nil {
			return err
		}
		data = append(data, b...)
	}

	hdr.Size = uint16(len(data) + NavHeaderLen + 7)

	b := NavV3Type[:]
	csum = updateCsum(csum, b)
	e.w.Write(b)

	b, _ = hdr.MarshalBinary()
	csum = updateCsum(csum, b)
	e.w.Write(b)

	csum = updateCsum(csum, data)
	e.w.Write(data)

	b = make([]byte, 4)
	ByteOrder.PutUint32(b, csum)
	_, err := e.w.Write(b)

	return err
}
